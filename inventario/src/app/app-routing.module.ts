import { CategoriasComponent } from './components/dashboard/categorias/categorias.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { VigilanteGuard } from './vigilante.guard';

const routes: Routes = [
  { path:'', redirectTo: 'login', pathMatch:'full'},
  { path:'login', component:LoginComponent },
  {
    path:'dashboard',
    loadChildren:()=> import('./components/dashboard/dashboard.module').then(x => x.DashboardModule),
    canActivate: [VigilanteGuard]
  },


  { path:'**', redirectTo: 'login', pathMatch:'full'},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
