import { Component, OnInit } from '@angular/core';
import { Categorias } from 'src/app/interfaces/categorias';
import { CategoriasService } from 'src/app/services/categorias.service';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-proveedores',
  templateUrl: './categorias.component.html',
  styleUrls: ['./categorias.component.css']
})
export class CategoriasComponent implements OnInit {
  
  categorias: Categorias[]=[];

  constructor(private service: CategoriasService) { }

  ngOnInit(): void {
    this.categorias = [];
    this.service.getCategorias()
    .subscribe(
      (data:any)=>{
        console.log(data);
        this.categorias = data.res;
      }
    )
  }

  borrarC(id:number){
    Swal.fire({
      title: '¿Estás seguro de eliminar esta categoria?',
      text: "¡No podrás revertir esto.!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminarlo!'
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire(
          '¡Eliminado!',
          'La categoria ha sido eliminada correctamente',
          'success'
        );
        this.service.deleteCategoria(id).
        subscribe(
          (data:any)=>{
            console.log("Categoria Borrada");
            console.log(data);
            this.ngOnInit();
          }
        )
      }
    });
  }

}
