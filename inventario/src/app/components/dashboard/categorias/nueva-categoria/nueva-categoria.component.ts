import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Categorias } from 'src/app/interfaces/categorias';
import { CategoriasService } from 'src/app/services/categorias.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-nueva-categoria',
  templateUrl: './nueva-categoria.component.html',
  styleUrls: ['./nueva-categoria.component.css']
})
export class NuevaCategoriaComponent implements OnInit {

  categories:Categorias[]=[];
  hide = true;

  form:FormGroup = this.fb.group({
    categoria:['',Validators.required],
    descripcion:['',Validators.required],
  })
  
  constructor(private fb: FormBuilder, private router: Router,
    private service: CategoriasService, private rutAct: ActivatedRoute) { }

    idCat:any;

  ngOnInit(): void {
     //Obtenemos el id del producto a editar
     this.idCat = this.rutAct.snapshot.params.id;
     console.log(this.idCat);

     if (this.idCat) {
      this.service.getCategoria(this.idCat).
      subscribe(
        (cate:any)=>{
          console.log(cate.res);
          this.cateModel = cate.res[0];
        }
      )
     }

  }

  cateModel:Categorias = new Categorias('','',0);

  guardarCate(){
    this.service.postCategoria(this.form.value).
    subscribe(
      (save:any)=>{
        console.log(save);
        this.router.navigateByUrl('/dashboard/categorias');
        this.getCategorias();
        
      }
    );
    Swal.fire(
      'Registro guardado',
      'You clicked the button!',
      'success'
      );
  }

   //Actualizar Categoria
   actualizar(idCate:number){
    this.service.updateCategoria(this.form.value,idCate).
      subscribe(
        (actualizado:any)=>{
          console.log(actualizado);
          this.router.navigateByUrl('/dashboard/categorias');
          this.getCategorias();
        }
      );
    Swal.fire(
      'Actualizado con exito',
      'You clicked the button!',
      'success'
    );
  }

   //Obtenemos las categorias
   getCategorias(){
    this.service.getCategorias().
    subscribe(
      (cate:any)=>{
        this.categories=cate.res;
        console.log(this.categories);
      }
    )
  }

}
