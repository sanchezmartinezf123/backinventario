import { Producto } from 'src/app/interfaces/productos';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { Component, OnInit,ViewChild } from '@angular/core';
import Swal from 'sweetalert2';
import { ActivatedRoute } from '@angular/router';
import { CategoriasService } from 'src/app/services/categorias.service';



@Component({
  selector: 'app-productos-categoria',
  templateUrl: './productos-categoria.component.html',
  styleUrls: ['./productos-categoria.component.css']
})
export class ProductosCategoriaComponent implements OnInit {
  listProductos: Producto[] = [];

  displayedColumns: string[] = ['id', 'producto', 'descripcion', 'precio', 'stock','unidad'];
  dataSource!: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(private service:CategoriasService, private rutAct: ActivatedRoute) { }
  idCate:any;
  cate:any;

  ngOnInit(): void {
    this.listProductos = [];
    this.idCate = this.rutAct.snapshot.params.id;
    console.log(this.idCate);

    //Obtenemos de que categoria es
    this.service.getCategoria(this.idCate).
    subscribe(
      (cate:any)=>{
        console.log(cate.res);
        this.cate = cate.res[0]["categoria"];
      }
    )

    //Obtenemos los productos
    this.service.prodByCategoria(this.idCate).
    subscribe(
      (res:any)=>{
        this.listProductos = res.res;
        this.dataSource = new MatTableDataSource(this.listProductos);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    )
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }


}