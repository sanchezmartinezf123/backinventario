import { ProductosCategoriaComponent } from './categorias/productos-categoria/productos-categoria.component';
import { NuevaCategoriaComponent } from './categorias/nueva-categoria/nueva-categoria.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CategoriasComponent } from './categorias/categorias.component';
import { DashboardComponent } from './dashboard.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { NuevoProductoComponent } from './home/nuevo-producto/nuevo-producto.component';
import { NuevoProveedorComponent } from './proveedores/nuevo-proveedor/nuevo-proveedor.component';
import { ProveedoresComponent } from './proveedores/proveedores.component';
import { CrearUsuarioComponent } from './usuarios/crear-usuario/crear-usuario.component';
import { UsuariosComponent } from './usuarios/usuarios.component';
import { VentasComponent } from './ventas/ventas.component';
import { NuevaVentaComponent } from './ventas/nueva-venta/nueva-venta.component';
import { ReportesComponent } from './reportes/reportes.component';

const routes: Routes = [
  { path: '',component: DashboardComponent, children: [
    {path: '',component: HomeComponent},
    {path: 'categorias',component: CategoriasComponent},
    {path: 'proveedores',component: ProveedoresComponent},
    {path: 'usuarios',component: UsuariosComponent},
    {path: 'productos',component: CategoriasComponent},
    {path: 'nuevo-producto',component: NuevoProductoComponent},
    {path: 'editar/:id', component:NuevoProductoComponent},
    {path: 'crear-usuario',component: CrearUsuarioComponent},
    {path: 'nuevo-proveedor',component: NuevoProveedorComponent},
    {path: 'editar-proveedor/:id',component: NuevoProveedorComponent},
    {path: 'home', component: HomeComponent},
    {path: 'ventas',component: VentasComponent},
    {path: 'reportes',component: ReportesComponent},
    {path: 'nueva-categoria',component: NuevaCategoriaComponent},
    {path: 'editar-categoria/:id',component: NuevaCategoriaComponent},
    {path: 'productos-categoria/:id',component: ProductosCategoriaComponent},
    {path: 'nueva-venta',component: NuevaVentaComponent},
    {path: 'header',component: HeaderComponent}
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
