import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { HomeComponent } from './home/home.component';
import { MenuComponent } from './menu/menu.component';
import { UsuariosComponent } from './usuarios/usuarios.component';
import { CategoriasComponent } from './categorias/categorias.component';
import { ProveedoresComponent } from './proveedores/proveedores.component';
import { SharedModule } from '../shared/shared.module';
import { NuevoProductoComponent } from './home/nuevo-producto/nuevo-producto.component';
import { CrearUsuarioComponent } from './usuarios/crear-usuario/crear-usuario.component';
import { NuevoProveedorComponent } from './proveedores/nuevo-proveedor/nuevo-proveedor.component';
import { VentasComponent } from './ventas/ventas.component';
import { ProductosCategoriaComponent } from './categorias/productos-categoria/productos-categoria.component';
import { HeaderComponent } from './header/header.component';
import { NuevaVentaComponent } from './ventas/nueva-venta/nueva-venta.component';
import { NuevaCategoriaComponent } from './categorias/nueva-categoria/nueva-categoria.component';
import { ProductosProveedorComponent } from './proveedores/productos-proveedor/productos-proveedor.component';
import { ReportesComponent } from './reportes/reportes.component';


@NgModule({
  declarations: [
    DashboardComponent,
    HomeComponent,
    MenuComponent,
    UsuariosComponent,
    CategoriasComponent,
    ProveedoresComponent,
    NuevoProductoComponent,
    NuevoProveedorComponent,
    NuevaCategoriaComponent,
    CrearUsuarioComponent,
    VentasComponent,
    ProductosCategoriaComponent,
    HeaderComponent,
    NuevaVentaComponent,
    ProductosProveedorComponent,
    ReportesComponent
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    SharedModule,
  ]
})
export class DashboardModule { }
