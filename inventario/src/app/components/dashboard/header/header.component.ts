import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

var name_user;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  @Output() toggleSidebarForMe: EventEmitter<any> = new EventEmitter();
  constructor(private router: Router) { }

  ngOnInit(): void {
    
  }

  name_user = localStorage.getItem("user");

  toggleSidebar() {
    this.toggleSidebarForMe.emit();
  }

  logout(){
    localStorage.clear();
  }
}
