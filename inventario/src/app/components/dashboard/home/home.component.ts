import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Producto } from 'src/app/interfaces/productos';
import { ProductosService } from 'src/app/services/productos.service';
import Swal from 'sweetalert2';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatBadge} from '@angular/material/badge';



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  listProductos: Producto[] = [];
  rol;
  roli
  panelOpenState = false;

  displayedColumns: string[] = ['id', 'producto', 'descripcion', 'precio', 'stock','unidad', 'options'];
  dataSource!: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
 
  constructor(private service:ProductosService,private snackBar: MatSnackBar) { }

    idProd:any;

  ngOnInit(): void {

    //Saber que tipo de usuario viene
    var rol = localStorage.getItem("rol");
    if (rol=='1') {
      this.roli = 'Administrador'
    }else{
      this.roli = 'Empleado'
    }

    this.listProductos = [];

    //Obtenemos los productos 
    this.service.getProductos().
    subscribe(
      (res:any)=>{
        this.listProductos = res.res;
        console.log(this.listProductos);
        this.dataSource = new MatTableDataSource(this.listProductos);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    )
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

 
  borrarProducto(id:number){
    
    Swal.fire({
      title: '¿Estás seguro de eliminar este producto?',
      text: "¡No podrás revertir esto.!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminarlo!'
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire(
          '¡Eliminado!',
          'El registro ha sido eliminado correctamente',
          'success'
        );
        this.service.deleteProducto(id).
        subscribe(
          (producto:any)=>{
            console.log("Producto Borrado");
            console.log(producto);
            this.ngOnInit();
          }
        )
      }
    });
  }

  openSnackBar(idProd:number) {
    this.service.getProducto(idProd).
    subscribe(
      (product:any)=>{
        console.log(product.res);
        var msj = "Marca: " + product.res[0]["marca"] + " Categoría: " + product.res[0]["categoria"] + " Proveedor: " + product.res[0]["proveedor"]
        this.snackBar.open(msj, product.res[0]["producto"]);
      }
    )
    
  }

}
