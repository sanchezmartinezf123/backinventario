import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Categorias } from 'src/app/interfaces/categorias';
import { Marcas } from 'src/app/interfaces/marcas.interface';
import { Producto } from 'src/app/interfaces/productos';
import { Proveedor } from 'src/app/interfaces/proveedores';
import { ProductosService } from 'src/app/services/productos.service';
import { ProveedoresService } from 'src/app/services/proveedores.service';
import Swal from 'sweetalert2';

var listaProductos: Producto[] = [];
var proveedores: Proveedor[] = []

@Component({
  selector: 'app-nuevo-producto',
  templateUrl: './nuevo-producto.component.html',
  styleUrls: ['./nuevo-producto.component.css']
})
export class NuevoProductoComponent implements OnInit {

  categories:Categorias[]=[];
  marcas: Marcas[]=[];
  dataSource = listaProductos;
  providers = proveedores;

  form:FormGroup = this.fb.group({
    producto:['',Validators.required],
    descripcion:['',Validators.required],
    precio:[,Validators.required],
    cantidad:[,Validators.required],
    unidad:[,Validators.required],
    id_categoria_producto:['',Validators.required],
    id_marca_producto:[,Validators.required],
    id_proveedor_producto:[,Validators.required],
  })

  constructor(private fb: FormBuilder, private router: Router,
    private service: ProductosService, private rutAct: ActivatedRoute, private SP:ProveedoresService
  ) {}

   idProd:any;

  ngOnInit(): void {
    this.getCategorias();
    this.getMarcas();
    this.getProveedores();
    //Obtenemos el id del producto a editar
    this.idProd = this.rutAct.snapshot.params.id;
    console.log(this.idProd);

    if(this.idProd){
      this.service.getProducto(this.idProd).
      subscribe(
        (product:any)=>{
          console.log(product.res);
          this.productoModel = product.res[0];
        }
      )
    }

  }

  productoModel:Producto = new Producto('','','','','',0,'','','');

  //Guardar Producto
  guardarProducto(){
    this.service.postProducto(this.form.value).
    subscribe(
      (save:any)=>{
        console.log(save);
        this.router.navigateByUrl('/dashboard');
        this.getProductos();
        
      }
    );
    Swal.fire(
      'Registro guardado',
      'You clicked the button!',
      'success'
      );
  }

  //Actualizar Producto
  actualizar(idProd:number){
    this.service.updateProducto(this.form.value,idProd).
      subscribe(
        (actualizado:any)=>{
          console.log(actualizado);
          this.router.navigateByUrl('/dashboard');
          this.getProductos();
        }
      );
    Swal.fire(
      'Actualizado con exito',
      'You clicked the button!',
      'success'
    );
  }

  //Obtenemos las marcas
  getMarcas(){
    this.service.getMarcas().
    subscribe(
      (mar:any)=>{
        this.marcas=mar.res;
        console.log(this.marcas);
      }
    )
  }

  //Obtenemos las categorias
  getCategorias(){
    this.service.getCategorias().
    subscribe(
      (cate:any)=>{
        this.categories=cate.res;
        console.log(this.categories);
      }
    )
  }

  //Obtener a los proveedores
  getProveedores(){
    this.SP.getProveedores().
    subscribe(
      (data:any)=>{
        this.providers = data.res;
        console.log(this.providers);
      }
    )
  }

  //Obtener todos los productos
  getProductos(){
    this.service.getProductos().
    subscribe(
      (data:any)=>{
        console.log(data);
      }
    )
  }

}
