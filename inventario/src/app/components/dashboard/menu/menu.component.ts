import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  rol;
  roli

  constructor() { }

  ngOnInit(): void {
     //Saber que tipo de usuario viene
     var rol = localStorage.getItem("rol");
     if (rol=='1') {
       this.roli = 'Administrador'
     }else{
       this.roli = 'Empleado'
     }

  }

  logout(){
    localStorage.clear();
  }

}
