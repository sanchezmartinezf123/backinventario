import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Proveedor } from 'src/app/interfaces/proveedores';
import { ProveedoresService } from 'src/app/services/proveedores.service';
import Swal from 'sweetalert2';

var proveedores: Proveedor[] = []

@Component({
  selector: 'app-nuevo-proveedor',
  templateUrl: './nuevo-proveedor.component.html',
  styleUrls: ['./nuevo-proveedor.component.css']
})
export class NuevoProveedorComponent implements OnInit {
  hide = true;

  providers = proveedores;
  idProv = 0;

  form:FormGroup = this.fb.group({
    proveedor:['',Validators.required],
    empresa:['',Validators.required],
    direccion:['',Validators.required],
    telefono:['',Validators.required],
    correo:['',Validators.required],
  })

  constructor(private fb:FormBuilder, private router:Router, private service:ProveedoresService, 
    private rutAct: ActivatedRoute) { }

  ngOnInit(): void {
    this.idProv = this.rutAct.snapshot.params.id
    console.log(this.idProv);

    if(this.idProv){
      this.service.getProveedor(this.idProv).
        subscribe(
          (prov:any)=>{
            console.log(prov.res[0]);
            this.provModel = prov.res[0];
          }
        )
    }
  }

  provModel:Proveedor = new Proveedor('','','','','',0);

  guardar(){
    this.service.postProveedor(this.form.value).
    subscribe(
      (data:any)=>{
        console.log(data);
      }
    )
    Swal.fire(
      'Proveedor guardado',
      'You clicked the button!',
      'success'
      );
      this.router.navigateByUrl('/dashboard/proveedores');
      this.getProveedores()
  }

  actualizar(id:number){
    console.log(this.form.value);
    this.service.updateProveedor(this.form.value,id).
    subscribe(
      (prov:any)=>{
        console.log(prov);
       
      }
    )
    Swal.fire(
      'Actualizado con exito',
      'You clicked the button!',
      'success'
    );
    this.router.navigateByUrl('/dashboard/proveedores');
    this.getProveedores()
  }

  getProveedores(){
    this.service.getProveedores().
    subscribe(
      (data:any)=>{
        console.log(data);
      }
    ) 
  }

}
