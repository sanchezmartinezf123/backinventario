import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Producto } from 'src/app/interfaces/productos';
import { ProductosService } from 'src/app/services/productos.service';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-productos-proveedor',
  templateUrl: './productos-proveedor.component.html',
  styleUrls: ['./productos-proveedor.component.css']
})
export class ProductosProveedorComponent implements OnInit {

  listProductos: Producto[] = [];

  displayedColumns: string[] = ['id', 'producto', 'descripcion', 'precio', 'stock','unidad'];
  dataSource!: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;


  constructor( @Inject(MAT_DIALOG_DATA) public data: any, private service:ProductosService) { }

  ngOnInit(): void {
    this.listProductos = [];
    console.log(this.data);
    var id_proveedor = this.data.id_proveedor;
    this.service.prodByProveedor(id_proveedor).
    subscribe(
      (data:any)=>{
        this.listProductos = data.res;
        this.dataSource = new MatTableDataSource(this.listProductos);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    )
  }

  
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
