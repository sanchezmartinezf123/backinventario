import { Component, OnInit } from '@angular/core';
import { Proveedor } from 'src/app/interfaces/proveedores';
import { ProveedoresService } from 'src/app/services/proveedores.service';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import Swal from 'sweetalert2';
import { ProductosProveedorComponent } from './productos-proveedor/productos-proveedor.component';

var proveedores: Proveedor[] = []

@Component({
  selector: 'app-proveedores',
  templateUrl: './proveedores.component.html',
  styleUrls: ['./proveedores.component.css']
})


export class ProveedoresComponent implements OnInit {

  providers = proveedores;

  constructor(private service: ProveedoresService,private dialog: MatDialog) { }

  ngOnInit(): void {

    this.providers = [];

    this.service.getProveedores().
    subscribe(
      (data:any)=>{
        this.providers = data.res;
        console.log(this.providers);
      }
    )
  }

  delete(id:number){
    Swal.fire({
      title: '¿Estás seguro de eliminar a este proveedor?',
      text: "¡No podrás revertir esto.!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminarlo!'
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire(
          '¡Eliminado!',
          'El proveedor ha sido eliminado correctamente',
          'success'
        );
        this.service.deleteProveedor(id).
        subscribe(
          (data:any)=>{
            console.log("Proveedor Borrado");
            console.log(data);
            this.ngOnInit();
          }
        )
      }
    });
  }

  productosProv(id_proveedor:number){
    console.log('Estos son los productos de este proveedor');
    this.dialog.open(ProductosProveedorComponent, {
      width:'50%',
      data:{
        id_proveedor:id_proveedor
      }
    });
  }

}
