import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder,Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Usuario } from 'src/app/interfaces/usuarios';
import { UsuariosService } from 'src/app/services/usuarios.service';

@Component({
  selector: 'app-crear-usuario',
  templateUrl: './crear-usuario.component.html',
  styleUrls: ['./crear-usuario.component.css']
})
export class CrearUsuarioComponent implements OnInit {

  hide = true;
  hide2 = true;
  email_usuario = new FormControl('', [Validators.required, Validators.email]);
  usuarioForm:FormGroup; 
  roles = [];
  
  constructor(private formBuilder: FormBuilder, private service: UsuariosService,
    @Inject(MAT_DIALOG_DATA) public data: any) { 
    this.usuarioForm = this.formBuilder.group({
      nombre_usuario   : ['',Validators.required],
      email_usuario    : ['',Validators.required],
      id_rol_usuario    : ['',Validators.required],
      password_usuario : ['',Validators.required],
      password2 : ['',Validators.required]
    })
  }

  ngOnInit(): void {
     //Obtener a los proveedores
      this.service.getRoles().
      subscribe(
        (data:any)=>{
          this.roles = data.res;
          console.log(this.roles);
        }
      )
  }

  getErrorMessageEmail() {
    if (this.email_usuario.hasError('required')) {
      return 'Debes ingresar un correo';
    }
    return this.email_usuario.hasError('email') ? 'No es un correo valido' : '';
  }

  guardar(){
    if (this.usuarioForm.value["password_usuario"] != this.usuarioForm.value["password2"]) {
      console.log("Las contraseñas no coinciden")
    }else{
        console.log(this.usuarioForm.value);
            this.service.postUsuario(this.usuarioForm.value).
            subscribe(
              (res:any)=>{
                console.log(res);
                this.getUsuarios();
              }
            )
    }
    
  }

  actualizar(id){
    console.log(id);
    console.log(this.usuarioForm.value);
    this.service.updateUsuario(this.usuarioForm.value,id).
    subscribe(
      (res:any)=>{
        console.log("Actualizado");
      }
    )
  }

  getUsuarios(){
    this.service.getUsuarios()
    .subscribe(
      (data:any)=>{
        console.log(data);
      }
    )
  }

}
