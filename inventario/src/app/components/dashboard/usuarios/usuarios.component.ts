import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Usuario } from 'src/app/interfaces/usuarios';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CrearUsuarioComponent } from './crear-usuario/crear-usuario.component';
import { UsuariosService } from 'src/app/services/usuarios.service';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.css']
})
export class UsuariosComponent implements OnInit {
  
  listaUsuarios:Usuario[] = [];
  
  displayedColumns: string[] = ['id_usuario', 'nombre', 'email', 'rol', 'acciones'];
  dataSource!: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  constructor(private dialog: MatDialog, private service: UsuariosService) { }

  ngOnInit(): void {

    this.listaUsuarios = [];

    this.service.getUsuarios().
    subscribe(
      (res:any)=>{
        this.listaUsuarios = res.res;
        console.log(this.listaUsuarios)
        this.dataSource = new MatTableDataSource(this.listaUsuarios);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    ) 
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  agregar() {
    this.dialog.open(CrearUsuarioComponent, {
      width:'30%',
      data:{
        nombre_usuario: '',
        email_usuario : '',
        id_rol_usuario: 1,
      }
    });
    this.ngOnInit();
  }  


  borrar(id:number){
    Swal.fire({
      title: '¿Estás seguro de eliminar a este usuario?',
      text: "¡No podrás revertir esto.!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminarlo!'
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire(
          '¡Eliminado!',
          'El usuario ha sido eliminado correctamente',
          'success'
        );
        this.service.deleteUsuario(id).
        subscribe(
          (data:any)=>{
            console.log("Usuario Borrado");
            console.log(data);
            this.ngOnInit();
          }
        )
      }
    });
  }

  editar(user:any){
    this.dialog.open(CrearUsuarioComponent, {
      width:'30%',
      data:{
        id_usuario:user.id_usuario,
        nombre_usuario: user.nombre_usuario,
        email_usuario : user.email_usuario,
        id_rol_usuario : user.id_rol_usuario,
      }
    });
  }

}
