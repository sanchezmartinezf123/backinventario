import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ProductosService } from 'src/app/services/productos.service';


@Component({
  selector: 'app-nueva-venta',
  templateUrl: './nueva-venta.component.html',
  styleUrls: ['./nueva-venta.component.css']
})
export class NuevaVentaComponent implements OnInit {
  ventaForm:FormGroup; 

  constructor(private formBuilder: FormBuilder,@Inject(MAT_DIALOG_DATA) public data: any, private service:ProductosService,
  private route:Router) {
    this.ventaForm = this.formBuilder.group({
      producto:['',Validators.required],
      cantidad:['',Validators.required]
    })
   }

  ngOnInit(): void {
  }

  salida(id:number){
    this.ventaForm.value['id_producto_reporte'] = id;
    this.ventaForm.value['tipo'] = 'Salida';
    this.service.postReporte(this.ventaForm.value).
    subscribe(
      (repor:any)=>{
        console.log(repor);
      }
    )
  this.ventaForm.value['cantidad'] = this.data.stock - this.ventaForm.value['cantidad'];
   console.log(this.ventaForm.value);
   this.service.venta(this.ventaForm.value,id).
   subscribe(
    (data:any)=>{
      console.log(data);
    }
    )
    
  }

  entrada(id:number){
    this.ventaForm.value['id_producto_reporte'] = id;
    this.ventaForm.value['tipo'] = 'Entrada';
    this.service.postReporte(this.ventaForm.value).
    subscribe(
      (repor:any)=>{
        console.log(repor);
      }
    )
    var int1 = parseFloat(this.ventaForm.value['cantidad']);
    var int2 = parseFloat(this.data.stock);
    this.ventaForm.value['cantidad'] = int1 + int2;
    console.log(this.ventaForm.value['cantidad']);
    this.service.venta(this.ventaForm.value,id).
    subscribe(
      (data:any)=>{
        console.log(data);
      }
      )
  }

}
