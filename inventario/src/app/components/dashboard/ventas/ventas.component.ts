import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Producto } from 'src/app/interfaces/productos';
import { ProductosService } from 'src/app/services/productos.service';
import { CrearUsuarioComponent } from '../usuarios/crear-usuario/crear-usuario.component';
import { NuevaVentaComponent } from './nueva-venta/nueva-venta.component';

@Component({
  selector: 'app-ventas',
  templateUrl: './ventas.component.html',
  styleUrls: ['./ventas.component.css']
})

export class VentasComponent implements OnInit {

  listProductos: Producto[] = [];

  displayedColumns: string[] = ['id', 'producto', 'descripcion', 'precio', 'stock', 'unidad', 'salida', 'entrada'];
  dataSource!: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(private dialog: MatDialog, private service:ProductosService) { }

  ngOnInit(): void {
    //Obtenemos los productos 
    this.service.getProductos().
    subscribe(
      (res:any)=>{
        this.listProductos = res.res;
        this.dataSource = new MatTableDataSource(this.listProductos);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    )
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }


  salida(prod:any) {
    this.dialog.open(NuevaVentaComponent, {
      width:'30%',
      data:{
        id_producto:prod.id_producto,
        producto:prod.producto,
        stock:prod.cantidad,
        cantidad_venta: '',
      }
    });
    this.ngOnInit()
  }  

  entrada(prod:any) {
    this.dialog.open(NuevaVentaComponent, {
      width:'30%',
      data:{
        id_producto:prod.id_producto,
        producto:prod.producto,
        stock:prod.cantidad,
        cantidad_venta: '',
        accion: 'entrada'
      }
    });
    this.ngOnInit()
  }  

}
