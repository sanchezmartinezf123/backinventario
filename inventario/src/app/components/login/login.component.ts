import { Component, OnInit } from '@angular/core';
import { resetFakeAsyncZone } from '@angular/core/testing';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { TouchSequence } from 'selenium-webdriver';
import { UsuariosService } from 'src/app/services/usuarios.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  loading = false;

  constructor(private fb: FormBuilder, private _snackBar: MatSnackBar, private router: Router,
    private service:UsuariosService) {
    this.form = this.fb.group({
      email_usuario:['',Validators.required],
      password_usuario:['',Validators.required]

    })
   }

  ngOnInit(): void {

  }
  ingresar(){

    this.service.login(this.form.value).
    subscribe(
      (res:any)=>{
        console.log(res);
        if(res){
          this.fakeloading();
          //Se guarda el token en localstorage
          localStorage.setItem("token",res.res[0]["token_usuario"])
          localStorage.setItem("user",res.res[0]["email_usuario"])
          localStorage.setItem("rol",res.res[0]["id_rol_usuario"])
        }else{
          this.error();
          this.form.reset();
        }
      }
    )
}
error(){
  this._snackBar.open('Usuario o contraseña son incorrectos', '',{
    duration:5000,
    horizontalPosition:'center',
    verticalPosition:'bottom'
  })
}
fakeloading(){
  this.loading = true;
  setTimeout(() => {
    this.router.navigate(['dashboard'])
  }, 1500);
}
}
