export class Categorias {
  constructor (
    public categoria: string,
    public descripcion_categoria: string,
    public id_categoria?: number,
  ){}
}