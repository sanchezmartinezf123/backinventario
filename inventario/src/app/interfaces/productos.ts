export class Producto {
    constructor (
      public producto: string,
      public descripcion: string,
      public precio: string,
      public cantidad: string,
      public unidad: string,
      public id_producto?: number,      
      public id_categoria_producto?: string,
      public id_marca_producto?: string,
      public id_proveedor_producto?: string
    ){}
  }