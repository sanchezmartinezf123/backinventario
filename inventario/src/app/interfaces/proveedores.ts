export class Proveedor {
    constructor (
        public proveedor: string,
        public empresa: string,
        public direccion: string,
        public telefono: string,
        public correo: string,
        public id_proveedor?: number
    ){}
  }