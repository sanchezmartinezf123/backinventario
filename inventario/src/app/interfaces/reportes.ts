export class Reporte {
    constructor (
      public id_producto_reporte: string,
      public cantidad:string,
      public tipo:string,
      public fecha:string,
      public id_reporte?:number

    ){}
  }