export interface Usuario {
    id_usuario?:number,
    nombre_usuario:string,
    email_usuario:string,
    password_usuario:string,
    token_usuario?:string,
    token_exp_usuario?:string
  }