import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Categorias } from '../interfaces/categorias';

@Injectable({
  providedIn: 'root'
})
export class CategoriasService {

  ruta = 'http://localhost/inventario/inventarioback/inventarioback/';
  token = localStorage.getItem("token");

  constructor(private http:HttpClient) { }

  //Obtener categorias
  getCategorias(){
    return this.http.get(`${this.ruta}categorias?select=*`)
  }

  //Obtener una categoria
  getCategoria(id:number){
    return this.http.get(`${this.ruta}categorias?linkTo=id_categoria&equalTo=${id}`);
  }

  //Guardar Categoria 
  postCategoria(cate: Categorias){
    const body = new HttpParams()
    .set(`categoria`,cate["categoria"])
    .set(`descripcion_categoria`,cate["descripcion"])

    const headers = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' });

    return this.http.post(`${this.ruta}categorias?token=${this.token}&table=usuarios&sufix=usuario`,body.toString(),{ headers })
  }

  //Actualizar categoria
  updateCategoria(cate: Categorias, id:number){
    const body = new HttpParams()
    .set(`categoria`,cate["categoria"])
    .set(`descripcion_categoria`,cate["descripcion"])

    const headers = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' });

    return this.http.put(`${this.ruta}categorias?id=${id}&nameId=id_categoria&token=${this.token}&table=usuarios&sufix=usuario`,body.toString(),{ headers })
  }

  //Elimianr categoria
  deleteCategoria(id:number){
    return this.http.delete(`${this.ruta}categorias?id=${id}&nameId=id_categoria&token=${this.token}&table=usuarios&sufix=usuario`)
  }

  //Obtener productos por categoria
  prodByCategoria(idCate:number){
    return this.http.get(`${this.ruta}productos?linkTo=id_categoria_producto&equalTo=${idCate}`)
  }

}
