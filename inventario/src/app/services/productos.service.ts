import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Producto } from '../interfaces/productos';
import { Reporte } from '../interfaces/reportes';

@Injectable({
  providedIn: 'root'
})
export class ProductosService {

  ruta = 'http://localhost/inventario/inventarioback/inventarioback/';
  token = localStorage.getItem("token");

  constructor(private http:HttpClient) { }

  //Obtener Productos
  getProductos(){
    return this.http.get(`${this.ruta}productos?select=id_producto,producto,descripcion,precio,cantidad,unidad`);   
  }

  //Obtener un producto
  getProducto(id:number){
    return this.http.get(`${this.ruta}relations?select=*&rel=productos,categorias,marcas,proveedores&type=producto,categoria,marca,proveedor&linkTo=id_producto&equalTo=${id}`);
   
  }

  //Guardar un producto
  postProducto(prod:Producto){
    const body = new HttpParams()
    .set(`producto`,prod["producto"])
    .set(`descripcion`,prod["descripcion"])
    .set(`precio`,prod["precio"])
    .set(`cantidad`,prod["cantidad"])
    .set(`unidad`,prod["unidad"])
    .set(`id_categoria_producto`,prod["id_categoria_producto"])
    .set(`id_marca_producto`,prod["id_marca_producto"])
    .set(`id_proveedor_producto`,prod["id_proveedor_producto"])

    const headers = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' });

   return this.http.post(`${this.ruta}productos?token=${this.token}&table=usuarios&sufix=usuario`,body.toString(),{ headers })

  }

  //Actualizar Producto
  updateProducto(prod:Producto,id:number){
    const body = new HttpParams()
    .set(`producto`,prod["producto"])
    .set(`descripcion`,prod["descripcion"])
    .set(`precio`,prod["precio"])
    .set(`cantidad`,prod["cantidad"])
    .set(`unidad`,prod["unidad"])
    .set(`id_categoria_producto`,prod["id_categoria_producto"])
    .set(`id_marca_producto`,prod["id_marca_producto"])
    .set(`id_proveedor_producto`,prod["id_proveedor_producto"])

    const headers = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' });

    return this.http.put(`${this.ruta}productos?id=${id}&nameId=id_producto&token=${this.token}&table=usuarios&sufix=usuario`,body.toString(),{ headers })
  }

  //Actualizar Producto
  venta(prod:Producto,id:number){
    const body = new HttpParams()
    .set(`producto`,prod["producto"])
    .set(`cantidad`,prod["cantidad"])
    const headers = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' });
    return this.http.put(`${this.ruta}productos?id=${id}&nameId=id_producto&token=${this.token}&table=usuarios&sufix=usuario`,body.toString(),{ headers })
  }

    //Guardar un reporte
    postReporte(repor:Reporte){
    const body = new HttpParams()
    .set(`id_producto_reporte`,repor["id_producto_reporte"])
    .set(`total`,repor["cantidad"])
    .set(`tipo`,repor["tipo"])
    const headers = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' });
  
     return this.http.post(`${this.ruta}reportes?token=${this.token}&table=usuarios&sufix=usuario`,body.toString(),{ headers })
  
    }
  

  //Eliminar Producto
  deleteProducto(id:number){
    return this.http.delete(`${this.ruta}productos?id=${id}&nameId=id_producto&token=${this.token}&table=usuarios&sufix=usuario`)
  }

  //Obtener Categorias
  getCategorias(){
    return this.http.get(`${this.ruta}categorias`);
  }

  //Obtener Marcas
  getMarcas(){
    return this.http.get(`${this.ruta}marcas`);
  }

  //Obtener productos por proveedor
  prodByProveedor(idProv:number){
    return this.http.get(`${this.ruta}productos?linkTo=id_proveedor_producto&equalTo=${idProv}`)
  }
  

}
