import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Proveedor } from '../interfaces/proveedores';

@Injectable({
  providedIn: 'root'
})
export class ProveedoresService {

  ruta = 'http://localhost/inventario/inventarioback/inventarioback/';
  token = localStorage.getItem("token");

  constructor(private http:HttpClient) { }

  //Obtener proveedores
  getProveedores(){
    return this.http.get(`${this.ruta}proveedores?select=*`)
  }

  //Obtener un proveedor
  getProveedor(id:number){
    return this.http.get(`${this.ruta}proveedores?linkTo=id_proveedor&equalTo=${id}`);
  }

  //Guardar un producto
  postProveedor(prov:Proveedor){
    const body = new HttpParams()
    .set(`proveedor`,prov["proveedor"])
    .set(`empresa`,prov["empresa"])
    .set(`direccion`,prov["direccion"])
    .set(`telefono`,prov["telefono"])
    .set(`correo`,prov["correo"])

    const headers = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' });

    return this.http.post(`${this.ruta}proveedores?token=${this.token}&table=usuarios&sufix=usuario`,body.toString(),{ headers })
  }

   //Actualizar Proveedor
   updateProveedor(prov:Proveedor,id:number){
    const body = new HttpParams()
    .set(`proveedor`,prov["proveedor"])
    .set(`empresa`,prov["empresa"])
    .set(`direccion`,prov["direccion"])
    .set(`telefono`,prov["telefono"])
    .set(`correo`,prov["correo"])

    const headers = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' });

    return this.http.put(`${this.ruta}proveedores?id=${id}&nameId=id_proveedor&token=${this.token}&table=usuarios&sufix=usuario`,body.toString(),{ headers })
  }

  //Eliminar Producto
  deleteProveedor(id:number){
    return this.http.delete(`${this.ruta}proveedores?id=${id}&nameId=id_proveedor&token=${this.token}&table=usuarios&sufix=usuario`)
  }


}
