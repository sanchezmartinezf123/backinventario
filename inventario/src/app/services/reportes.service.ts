import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ReportesService {

  ruta = 'http://localhost/inventario/inventarioback/inventarioback/';
  token = localStorage.getItem("token");

  constructor(private http:HttpClient) { }

   //Obtener todos los reportes
   getReportes(tipo:string){
    return this.http.get(`${this.ruta}relations?select=id_producto,producto,precio,tipo,fecha,SUM(total) as suma,SUM(total)*precio as monto&rel=reportes,productos&type=reporte,producto&linkTo=tipo&equalTo=${tipo}&orderBy=SUM(total)&orderMode=DESC&gm=id_producto,fecha`);   
  
  }

}
