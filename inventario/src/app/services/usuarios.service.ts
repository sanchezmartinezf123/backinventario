import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Usuario } from '../interfaces/usuarios';

@Injectable({
  providedIn: 'root'
})
export class UsuariosService {

 

  ruta = 'http://localhost/inventario/inventarioback/inventarioback/';
  token = localStorage.getItem("token");

  constructor(private http:HttpClient) { }

  login(user:Usuario){

    //Se pone esto para pasar el JSON a formato x-www-form-urlencoded
   const body = new HttpParams() 
    //Se obtienen los parametros del body y se guardan en nuevas variables  
    .set(`email_usuario`,user["email_usuario"])
    .set(`password_usuario`,user["password_usuario"]);

    //Header para x-www-form-urlencoded
    const headers = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' });

    //Petición post
    return this.http.post(`${this.ruta}usuarios?login=true&sufix=usuario`,body.toString(),{ headers })

  }

  //Obtener todos los usuarios
  getUsuarios(){
    return this.http.get(`${this.ruta}relations?select=*&rel=usuarios,roles&type=usuario,rol`);    
    
  }

  //Obtener todos los usuarios
  getRoles(){
    return this.http.get(`${this.ruta}roles?select=*`);    
    
  }

  //Guardar un Usuario
  postUsuario(user:Usuario){
    const body = new HttpParams()
    .set(`nombre_usuario`,user["nombre_usuario"])
    .set(`email_usuario`,user["email_usuario"])
    .set(`id_rol_usuario`,user["id_rol_usuario"])
    .set(`password_usuario`,user["password_usuario"])
  
    const headers = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' });

   return this.http.post(`${this.ruta}usuarios?register=true&sufix=usuario`,body.toString(),{ headers })

  }

  //Actualizar Usuario
  updateUsuario(user:Usuario,id:number){
    const body = new HttpParams()
    .set(`nombre_usuario`,user["nombre_usuario"])
    .set(`email_usuario`,user["email_usuario"])
    .set(`id_rol_usuario`,user["id_rol_usuario"])
    .set(`password_usuario`,user["password_usuario"])

    const headers = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' });

    return this.http.put(`${this.ruta}usuarios?id=${id}&nameId=id_usuario&token=${this.token}&table=usuarios&sufix=usuario`,body.toString(),{ headers })
  }

  //Eliminar un usuairo
  deleteUsuario(id:number){
    return this.http.delete(`${this.ruta}usuarios?id=${id}&nameId=id_usuario&token=${this.token}&table=usuarios&sufix=usuario`)
  }

}
